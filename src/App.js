import "./App.css";
import "antd/dist/antd.css";
import { Layout, Menu } from "antd";
import React from "react";
import { Route, Switch, useHistory, useLocation } from "react-router-dom";
import styled from "styled-components";

import { fruits } from "./data/fruits";
import { icecream } from "./data/icecream";
import ProductPage from "./pages/product-page";

const { Sider } = Layout;

const App = () => {
  const history = useHistory();
  const location = useLocation();

  if (location.pathname === "/fruits") {
    location.state = {
      id: "1",
    };
  } else if (location.pathname === "/icecream") {
    location.state = {
      id: "2",
    };
  }

  return (
    <OuterContainer>
      <Sider
        width={200}
        style={{ height: "100%" }}
        className="site-layout-background"
      >
        <Menu
          theme="dark"
          mode="inline"
          style={{ height: "100%", borderRight: 0 }}
          selectedKeys={location.state && location.state.id}
        >
          <Menu.Item onClick={() => history.push("/fruits")} key="1">
            Frutas
          </Menu.Item>
          <Menu.Item onClick={() => history.push("/icecream")} key="2">
            Sorvetes
          </Menu.Item>
        </Menu>
      </Sider>

      <Switch>
        <Route path="/fruits">
          <ProductPage products={fruits} />
        </Route>
        <Route path="/icecream">
          <ProductPage products={icecream} />
        </Route>
      </Switch>
      <ContentContainer />
    </OuterContainer>
  );
};

const OuterContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
`;

const ContentContainer = styled.div`
  max-height: 100vh;
  padding: 48px;
`;

export default App;
