export const icecream = [
  {
    name: "Netflix & Chilll'd ",
    price: "R$ 19,00",
    img:
      "https://www.benandjerry.com.br/files/live/sites/systemsite/files/flavors/products/br/pints/netflix-chilld-detail.png",
  },
  {
    name: "Doce de leite",
    price: "R$ 19,00",
    img:
      "https://www.benandjerry.com.br/files/live/sites/systemsite/files/flavors/products/br/pints/doce-deleite-core-detail.png",
  },
  {
    name: "Strawberry Cheesecake",
    price: "R$ 19,00",
    img:
      "https://www.benandjerry.com.br/files/live/sites/systemsite/files/flavors/products/br/pints/strawberry-cheesecake-detail-open.png",
  },
  {
    name: "Cone Sweet Cone",
    price: "R$ 19,00",
    img:
      "https://www.benandjerry.com.br/files/live/sites/systemsite/files/flavors/products/br/pints/cone-sweet-cone-detail.png",
  },
];
